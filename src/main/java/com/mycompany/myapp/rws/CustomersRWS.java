package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.CustomersDTO;
import com.mycompany.myapp.entity.Customers;
import com.mycompany.myapp.service.CustomersService;


@RestController
@CrossOrigin(origins = "http://localhost:8091")
public class CustomersRWS 
{	
	private static final Logger logger = LoggerFactory.getLogger(CustomersRWS.class);
	
	@Autowired 
	CustomersService customersService;
	
	@RequestMapping(value = "/customer/{id}", method = RequestMethod.GET)
	public ResponseEntity<CustomersDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing customer with id: " + id);
		return new ResponseEntity<CustomersDTO>(customersService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/customers", method = RequestMethod.GET)
	public ResponseEntity<List<Customers>> findAll() {
		logger.info("Listing all ");
		return new ResponseEntity<List<Customers>>(customersService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/customers/{pageNum}/{pageSize}", method = RequestMethod.GET)
	public ResponseEntity<Page<Customers>> findAllWithPaging(@PathVariable Integer pageNum, @PathVariable Integer pageSize) {
		logger.info("Listing all customers with paging");
		return new ResponseEntity<Page<Customers>>(customersService.findAllWithPaging(pageNum, pageSize), HttpStatus.OK);
	}

}
