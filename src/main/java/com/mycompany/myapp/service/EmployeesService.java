package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.myapp.domain.EmployeesDTO;
import com.mycompany.myapp.entity.Employees;

public interface EmployeesService
{
	long count();
	EmployeesDTO findOne(Integer id);
	List<Employees> findAll();
	Page<Employees> findAllWithPaging(int page, int size);
}
