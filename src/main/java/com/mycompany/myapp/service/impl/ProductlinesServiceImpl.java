package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.ProductlinesDTO;
import com.mycompany.myapp.entity.Productlines;
import com.mycompany.myapp.repository.ProductlinesRepository;
import com.mycompany.myapp.service.ProductlinesService;

@Service
public class ProductlinesServiceImpl implements ProductlinesService 
{
	@Autowired
	ProductlinesRepository productlinesRepository;

	@Transactional
	public long count() 
	{		
		return productlinesRepository.count();		
	}
	
	@Transactional
	public ProductlinesDTO findOne(String id) 
	{		
		Productlines productline = productlinesRepository.findOne(id);	
		return new ProductlinesDTO(productline.getProductLine(), productline.getTextDescription());
	}
	
	@Transactional
	public List<ProductlinesDTO> findAll() 
	{
		List<ProductlinesDTO> ret = new ArrayList<ProductlinesDTO>();
		for(Productlines productline : productlinesRepository.findAll())
		{
			ret.add(new ProductlinesDTO(productline.getProductLine(), productline.getTextDescription()));		
		}		
		return ret;		
	}

	
	

}
