package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.myapp.domain.OfficesDTO;
import com.mycompany.myapp.entity.Offices;

public interface OfficesService
{	
	long count();
	OfficesDTO findOne(String id);
	List<Offices> findAll();
	Page<Offices> findAllWithPaging(int page, int size);
	void save(OfficesDTO officesDTO);

}
