package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mycompany.myapp.entity.Payments;
import com.mycompany.myapp.entity.PaymentsPK;

public interface PaymentsRepository extends JpaRepository<Payments, PaymentsPK> {

}
