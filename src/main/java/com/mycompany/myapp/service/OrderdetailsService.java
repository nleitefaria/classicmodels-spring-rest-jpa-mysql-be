package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.myapp.domain.OrderdetailsDTO;
import com.mycompany.myapp.entity.Orderdetails;

public interface OrderdetailsService 
{	
	long count() ;
	OrderdetailsDTO findOne(Integer orderNumber, String checkNumber);
	List<Orderdetails> findAll() ;
	Page<Orderdetails> findAllWithPaging(int page, int size);

}
