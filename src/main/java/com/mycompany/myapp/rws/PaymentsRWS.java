package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.PaymentsDTO;
import com.mycompany.myapp.service.PaymentsService;

@RestController
@CrossOrigin(origins = "http://localhost:8091")
public class PaymentsRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(CustomersRWS.class);
	
	@Autowired 
	PaymentsService paymentsService;
	
	@RequestMapping(value = "/payment/{customerNumber}/{checkNumber}", method = RequestMethod.GET)
	public ResponseEntity<PaymentsDTO> findOne(@PathVariable Integer customerNumber, @PathVariable String checkNumber)
	{
		logger.info("Listing customer with id: " + customerNumber + " - "  + checkNumber);
		return new ResponseEntity<PaymentsDTO>(paymentsService.findOne(customerNumber, checkNumber), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/payments", method = RequestMethod.GET)
	public ResponseEntity<List<PaymentsDTO>> findAll() {
		logger.info("Listing all payments");
		return new ResponseEntity<List<PaymentsDTO>>(paymentsService.findAll(), HttpStatus.OK);
	}

}
