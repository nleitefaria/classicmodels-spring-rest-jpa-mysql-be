package com.mycompany.myapp.domain;

import java.math.BigDecimal;

public class OrderdetailsDTO 
{	
	protected OrderdetailsPKDTO orderdetailsPK;
    private int quantityOrdered;
    private BigDecimal priceEach;
    private short orderLineNumber;
    
	public OrderdetailsDTO() 
	{	
	}

	public OrderdetailsDTO(OrderdetailsPKDTO orderdetailsPK, int quantityOrdered, BigDecimal priceEach, short orderLineNumber) 
	{		
		this.orderdetailsPK = orderdetailsPK;
		this.quantityOrdered = quantityOrdered;
		this.priceEach = priceEach;
		this.orderLineNumber = orderLineNumber;
	}

	public OrderdetailsPKDTO getOrderdetailsPK() {
		return orderdetailsPK;
	}

	public void setOrderdetailsPK(OrderdetailsPKDTO orderdetailsPK) {
		this.orderdetailsPK = orderdetailsPK;
	}

	public int getQuantityOrdered() {
		return quantityOrdered;
	}

	public void setQuantityOrdered(int quantityOrdered) {
		this.quantityOrdered = quantityOrdered;
	}

	public BigDecimal getPriceEach() {
		return priceEach;
	}

	public void setPriceEach(BigDecimal priceEach) {
		this.priceEach = priceEach;
	}

	public short getOrderLineNumber() {
		return orderLineNumber;
	}

	public void setOrderLineNumber(short orderLineNumber) {
		this.orderLineNumber = orderLineNumber;
	}
}
