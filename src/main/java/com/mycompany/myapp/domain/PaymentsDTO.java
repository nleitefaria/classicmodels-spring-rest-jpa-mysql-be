package com.mycompany.myapp.domain;

import java.math.BigDecimal;
import java.util.Date;

public class PaymentsDTO {
	
	protected PaymentsPKDTO paymentsPK;
    private Date paymentDate;
    private BigDecimal amount;
    
	public PaymentsDTO() {
	}

	public PaymentsDTO(PaymentsPKDTO paymentsPK, Date paymentDate, BigDecimal amount) {
		this.paymentsPK = paymentsPK;
		this.paymentDate = paymentDate;
		this.amount = amount;
	}

	public PaymentsPKDTO getPaymentsPK() {
		return paymentsPK;
	}

	public void setPaymentsPK(PaymentsPKDTO paymentsPK) {
		this.paymentsPK = paymentsPK;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
