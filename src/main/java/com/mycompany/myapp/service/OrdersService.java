package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.myapp.domain.OrdersDTO;
import com.mycompany.myapp.entity.Orders;

public interface OrdersService
{
	long count();
	OrdersDTO findOne(Integer id);
	List<Orders> findAll();
	Page<Orders> findAllWithPaging(int page, int size);
}
