package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.ProductsDTO;
import com.mycompany.myapp.service.ProductsService;

@RestController
@CrossOrigin(origins = "http://localhost:8091")
public class ProductsRWS
{
	private static final Logger logger = LoggerFactory.getLogger(ProductsRWS.class);
	
	@Autowired 
	ProductsService productsService;
	
	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProductsDTO> findOne(@PathVariable String id)
	{
		logger.info("Listing product with id: " + id);
		return new ResponseEntity<ProductsDTO>(productsService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public ResponseEntity<List<ProductsDTO>> findAll() {
		logger.info("Listing all officess");
		return new ResponseEntity<List<ProductsDTO>>(productsService.findAll(), HttpStatus.OK);
	}

}
