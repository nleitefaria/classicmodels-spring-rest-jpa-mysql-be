package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.entity.Orderdetails;
import com.mycompany.myapp.entity.OrderdetailsPK;

@Repository
public interface OrderdetailsRepository extends JpaRepository<Orderdetails, OrderdetailsPK> {

}
