package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.OrdersDTO;
import com.mycompany.myapp.entity.Orders;
import com.mycompany.myapp.service.OrdersService;

@RestController
@CrossOrigin(origins = "http://localhost:8091")
public class OrdersRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(OrdersRWS.class);
	
	@Autowired 
	OrdersService ordersService;
	
	@RequestMapping(value = "/order/{id}", method = RequestMethod.GET)
	public ResponseEntity<OrdersDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing order with id: " + id);
		return new ResponseEntity<OrdersDTO>(ordersService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	public ResponseEntity<List<Orders>> findAll() {
		logger.info("Listing all orders");
		return new ResponseEntity<List<Orders>>(ordersService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/orders/{pageNum}/{pageSize}", method = RequestMethod.GET)
	public ResponseEntity<Page<Orders>> findAllWithPaging(@PathVariable Integer pageNum, @PathVariable Integer pageSize) {
		logger.info("Listing all orders with paging");
		return new ResponseEntity<Page<Orders>>(ordersService.findAllWithPaging(pageNum, pageSize), HttpStatus.OK);
	}

}
