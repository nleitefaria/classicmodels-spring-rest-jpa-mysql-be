package com.mycompany.myapp.domain;


public class PaymentsPKDTO {
	
	private int customerNumber;
    private String checkNumber;
    
	public PaymentsPKDTO() {
		
	}

	public PaymentsPKDTO(int customerNumber, String checkNumber) {
		this.customerNumber = customerNumber;
		this.checkNumber = checkNumber;
	}

	public int getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}
}
