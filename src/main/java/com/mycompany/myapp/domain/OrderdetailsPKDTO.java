package com.mycompany.myapp.domain;

public class OrderdetailsPKDTO 
{	
	private int orderNumber;
    private String productCode;
    
	public OrderdetailsPKDTO() {
		
	}

	public OrderdetailsPKDTO(int orderNumber, String productCode) {
		this.orderNumber = orderNumber;
		this.productCode = productCode;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

}
