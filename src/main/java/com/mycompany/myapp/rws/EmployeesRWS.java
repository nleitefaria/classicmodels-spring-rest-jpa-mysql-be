package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.EmployeesDTO;
import com.mycompany.myapp.entity.Employees;
import com.mycompany.myapp.service.EmployeesService;

@RestController
@CrossOrigin(origins = "http://localhost:8091")
public class EmployeesRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(EmployeesRWS.class);
	
	@Autowired 
	EmployeesService employeesService;
	
	@RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
	public ResponseEntity<EmployeesDTO> findOne(@PathVariable Integer id)
	{
		logger.info("Listing employee with id: " + id);
		return new ResponseEntity<EmployeesDTO>(employeesService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	public ResponseEntity<List<Employees>> findAll() {
		logger.info("Listing all employees");
		return new ResponseEntity<List<Employees>>(employeesService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/employees/{pageNum}/{pageSize}", method = RequestMethod.GET)
	public ResponseEntity<Page<Employees>> findAllWithPaging(@PathVariable Integer pageNum, @PathVariable Integer pageSize) {
		logger.info("Listing all employees with paging");
		return new ResponseEntity<Page<Employees>>(employeesService.findAllWithPaging(pageNum, pageSize), HttpStatus.OK);
	}

}
