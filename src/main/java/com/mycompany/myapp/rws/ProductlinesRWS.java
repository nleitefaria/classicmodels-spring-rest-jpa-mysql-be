package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.ProductlinesDTO;
import com.mycompany.myapp.service.ProductlinesService;

@RestController
@CrossOrigin(origins = "http://localhost:8091")
public class ProductlinesRWS
{	
	private static final Logger logger = LoggerFactory.getLogger(ProductlinesRWS.class);
	
	@Autowired 
	ProductlinesService productlinesService;
	
	@RequestMapping(value = "/productline/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProductlinesDTO> findOne(@PathVariable String id)
	{
		logger.info("Listing productline with id: " + id);
		return new ResponseEntity<ProductlinesDTO>(productlinesService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/productlines", method = RequestMethod.GET)
	public ResponseEntity<List<ProductlinesDTO>> findAll() {
		logger.info("Listing all ");
		return new ResponseEntity<List<ProductlinesDTO>>(productlinesService.findAll(), HttpStatus.OK);
	}
	
	

}
