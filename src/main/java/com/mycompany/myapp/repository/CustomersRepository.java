package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.entity.Customers;

@Repository
public interface CustomersRepository extends JpaRepository<Customers, Integer> {

}
