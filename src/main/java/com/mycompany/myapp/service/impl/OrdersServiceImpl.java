package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.OrdersDTO;
import com.mycompany.myapp.entity.Orders;
import com.mycompany.myapp.repository.OrdersRepository;
import com.mycompany.myapp.service.OrdersService;

@Service
public class OrdersServiceImpl implements OrdersService {
	
	@Autowired
	OrdersRepository ordersRepository;

	@Transactional
	public long count() 
	{		
		return ordersRepository.count();		
	}
	
	@Transactional
	public OrdersDTO findOne(Integer id) 
	{		
		Orders order = ordersRepository.findOne(id);	
		return new OrdersDTO(order.getOrderNumber(), order.getOrderDate(), order.getRequiredDate(), order.getShippedDate(), order.getStatus(), order.getComments());
	}
	
	@Transactional
	public List<Orders> findAll() 
	{
		return ordersRepository.findAll();
	}
	
	@Transactional
	public Page<Orders> findAllWithPaging(int page, int size) 
	{
		return ordersRepository.findAll(createPageRequest(page, size));
	}
	
	private Pageable createPageRequest(int page, int size)
	{
	    return new PageRequest(page - 1, size);
	}

}
