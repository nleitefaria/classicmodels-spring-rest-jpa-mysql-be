package com.mycompany.myapp.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.mycompany.myapp.domain.CustomersDTO;
import com.mycompany.myapp.entity.Customers;

public interface CustomersService 
{
	long count();
	CustomersDTO findOne(Integer id);
	List<Customers> findAll();
	Page<Customers> findAllWithPaging(int page, int size);

}
