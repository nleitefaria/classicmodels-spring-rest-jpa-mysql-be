package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.OfficesDTO;
import com.mycompany.myapp.entity.Offices;
import com.mycompany.myapp.service.OfficesService;

@RestController
@CrossOrigin(origins = {"http://localhost:8091", "http://localhost"})
public class OfficesRWS {
	
	private static final Logger logger = LoggerFactory.getLogger(OfficesRWS.class);
	
	@Autowired 
	OfficesService officesService;
	
	@RequestMapping(value = "/office/{id}", method = RequestMethod.GET)
	public ResponseEntity<OfficesDTO> findOne(@PathVariable String id)
	{
		logger.info("Listing office with id: " + id);
		return new ResponseEntity<OfficesDTO>(officesService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/offices", method = RequestMethod.GET)
	public ResponseEntity<List<Offices>> findAll() {
		logger.info("Listing all offices");
		return new ResponseEntity<List<Offices>>(officesService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/offices/{pageNum}/{pageSize}", method = RequestMethod.GET)
	public ResponseEntity<Page<Offices>> findAllWithPaging(@PathVariable Integer pageNum, @PathVariable Integer pageSize) {
		logger.info("Listing all offices with paging");
		return new ResponseEntity<Page<Offices>>(officesService.findAllWithPaging(pageNum, pageSize), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/office", method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody OfficesDTO officesDTO)
	{       
        logger.info("Creating office"); 	
		try
		{
			officesService.save(officesDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
    }

}
