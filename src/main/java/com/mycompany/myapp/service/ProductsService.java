package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.ProductsDTO;

public interface ProductsService
{	
	long count();
	ProductsDTO findOne(String id);
	List<ProductsDTO> findAll();

}
