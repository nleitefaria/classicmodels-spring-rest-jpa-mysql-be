package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.OfficesDTO;
import com.mycompany.myapp.entity.Offices;
import com.mycompany.myapp.repository.OfficesRepository;
import com.mycompany.myapp.service.OfficesService;

@Service
public class OfficesServiceImpl implements OfficesService
{
	@Autowired
	OfficesRepository officesRepository;

	@Transactional
	public long count() 
	{		
		return officesRepository.count();		
	}
	
	@Transactional
	public OfficesDTO findOne(String id) 
	{		
		Offices office = officesRepository.findOne(id);	
		return new OfficesDTO(office.getOfficeCode(), office.getCity(), office.getPhone(), office.getAddressLine1(), office.getAddressLine2(), office.getState(), office.getCountry(), office.getPostalCode(), office.getTerritory());
	}
	
	@Transactional
	public List<Offices> findAll() 
	{
		return officesRepository.findAll();
	}
	
	@Transactional
	public Page<Offices> findAllWithPaging(int page, int size) 
	{
		return officesRepository.findAll(createPageRequest(page, size));
	}
	
	@Transactional
	public void save(OfficesDTO officesDTO) 
	{	
		Offices offices = new Offices(officesDTO.getOfficeCode(), officesDTO.getCity(), officesDTO.getPhone(), officesDTO.getAddressLine1(), officesDTO.getCountry(), officesDTO.getPostalCode(), officesDTO.getTerritory());
		officesRepository.save(offices);		
	}
		
	private Pageable createPageRequest(int page, int size)
	{
	    return new PageRequest(page - 1, size);
	}
}
