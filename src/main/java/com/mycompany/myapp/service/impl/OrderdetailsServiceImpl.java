package com.mycompany.myapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.OrderdetailsDTO;
import com.mycompany.myapp.domain.OrderdetailsPKDTO;
import com.mycompany.myapp.entity.Orderdetails;
import com.mycompany.myapp.entity.OrderdetailsPK;
import com.mycompany.myapp.repository.OrderdetailsRepository;
import com.mycompany.myapp.service.OrderdetailsService;

@Service
public class OrderdetailsServiceImpl implements OrderdetailsService
{
	
	@Autowired
	OrderdetailsRepository orderdetailsRepository;

	@Transactional
	public long count() 
	{		
		return orderdetailsRepository.count();		
	}
	
	@Transactional
	public OrderdetailsDTO findOne(Integer orderNumber, String checkNumber) 
	{	
		OrderdetailsPK id = new OrderdetailsPK(orderNumber, checkNumber);
		Orderdetails orderdetails = orderdetailsRepository.findOne(id);	
		OrderdetailsPKDTO orderdetailsPKDTO = new OrderdetailsPKDTO(orderdetails.getOrderdetailsPK().getOrderNumber(), orderdetails.getOrderdetailsPK().getProductCode());	
		return new OrderdetailsDTO(orderdetailsPKDTO, orderdetails.getQuantityOrdered(), orderdetails.getPriceEach(), orderdetails.getOrderLineNumber());
	}

	@Transactional
	public List<Orderdetails> findAll() 
	{				
		return orderdetailsRepository.findAll();
	}
	
	@Transactional
	public Page<Orderdetails> findAllWithPaging(int page, int size) 
	{
		return orderdetailsRepository.findAll(createPageRequest(page, size));
	}
	
	private Pageable createPageRequest(int page, int size)
	{
	    return new PageRequest(page - 1, size);
	}

	
}
