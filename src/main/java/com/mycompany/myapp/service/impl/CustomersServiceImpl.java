package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.CustomersDTO;
import com.mycompany.myapp.entity.Customers;

import com.mycompany.myapp.repository.CustomersRepository;
import com.mycompany.myapp.service.CustomersService;

@Service
public class CustomersServiceImpl implements CustomersService{
	
	@Autowired
	CustomersRepository customersRepository;

	@Transactional
	public long count() 
	{		
		return customersRepository.count();		
	}
	
	@Transactional
	public CustomersDTO findOne(Integer id) 
	{		
		Customers customer = customersRepository.findOne(id);	
		return new CustomersDTO(customer.getCustomerNumber(), customer.getCustomerName(), customer.getContactLastName(), customer.getContactFirstName(), customer.getPhone(), customer.getAddressLine1(), customer.getAddressLine2(), customer.getCity(), customer.getState(), customer.getPostalCode(), customer.getCountry(), customer.getCreditLimit());
	}
		
	@Transactional
	public List<Customers> findAll() 
	{		
		return customersRepository.findAll();
	}
	
	@Transactional
	public Page<Customers> findAllWithPaging(int page, int size) 
	{
		return customersRepository.findAll(createPageRequest(page, size));
	}
	
	private Pageable createPageRequest(int page, int size)
	{
	    return new PageRequest(page - 1, size);
	}
	

}
