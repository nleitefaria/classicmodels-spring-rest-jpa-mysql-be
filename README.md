My implementation of Restful Webservices using Spring Boot + Spring Data JPA over the MySQL sample Classic Models database.


![picture](https://www.ntu.edu.sg/home/ehchua/programming/sql/images/SampleClassicmodels.png)