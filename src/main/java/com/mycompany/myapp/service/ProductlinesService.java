package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.ProductlinesDTO;

public interface ProductlinesService 
{
	long count();
	ProductlinesDTO findOne(String id);
	List<ProductlinesDTO> findAll();

}
