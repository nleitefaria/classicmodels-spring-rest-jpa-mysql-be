package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.PaymentsDTO;

public interface PaymentsService {
	
	long count();
	PaymentsDTO findOne(Integer customerNumber, String checkNumber);
	List<PaymentsDTO> findAll();


}
