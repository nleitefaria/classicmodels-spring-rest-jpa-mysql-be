package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.EmployeesDTO;
import com.mycompany.myapp.entity.Employees;
import com.mycompany.myapp.repository.EmployeesRepository;
import com.mycompany.myapp.service.EmployeesService;

@Service
public class EmployeesServiceImpl implements EmployeesService
{
	
	@Autowired
	EmployeesRepository employeesRepository;

	@Transactional
	public long count() 
	{		
		return employeesRepository.count();		
	}
	
	@Transactional
	public EmployeesDTO findOne(Integer id) 
	{		
		Employees employees = employeesRepository.findOne(id);	
		return new EmployeesDTO(employees.getEmployeeNumber(), employees.getLastName(), employees.getFirstName(), employees.getExtension(), employees.getEmail(), employees.getJobTitle());
	}
	
//	@Transactional
//	public List<EmployeesDTO> findAll() 
//	{
//		List<EmployeesDTO> ret = new ArrayList<EmployeesDTO>();
//		for(Employees employees : employeesRepository.findAll())
//		{
//			ret.add(new EmployeesDTO(employees.getEmployeeNumber(), employees.getLastName(), employees.getFirstName(), employees.getExtension(), employees.getEmail(), employees.getJobTitle()));		
//		}		
//		return ret;		
//	}
	
	@Transactional
	public List<Employees> findAll() 
	{		
		return employeesRepository.findAll();
	}
	
	@Transactional
	public Page<Employees> findAllWithPaging(int page, int size) 
	{
		return employeesRepository.findAll(createPageRequest(page, size));
	}
	
	private Pageable createPageRequest(int page, int size) {
	    return new PageRequest(page - 1, size);
	}

}
