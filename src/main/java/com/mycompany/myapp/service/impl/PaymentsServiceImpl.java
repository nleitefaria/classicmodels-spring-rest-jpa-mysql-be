package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.CustomersDTO;
import com.mycompany.myapp.domain.PaymentsDTO;
import com.mycompany.myapp.domain.PaymentsPKDTO;
import com.mycompany.myapp.entity.Customers;
import com.mycompany.myapp.entity.Payments;
import com.mycompany.myapp.entity.PaymentsPK;
import com.mycompany.myapp.repository.CustomersRepository;
import com.mycompany.myapp.repository.PaymentsRepository;
import com.mycompany.myapp.service.PaymentsService;

@Service
public class PaymentsServiceImpl implements PaymentsService{
	
	@Autowired
	PaymentsRepository paymentsRepository;

	@Transactional
	public long count() 
	{		
		return paymentsRepository.count();		
	}
	
	@Transactional
	public PaymentsDTO findOne(Integer customerNumber, String checkNumber) 
	{	
		PaymentsPK id = new PaymentsPK(customerNumber, checkNumber);	
		Payments payments = paymentsRepository.findOne(id);		
		PaymentsPKDTO paymentsPKDTO = new PaymentsPKDTO(payments.getPaymentsPK().getCustomerNumber(), payments.getPaymentsPK().getCheckNumber());
		return new PaymentsDTO(paymentsPKDTO, payments.getPaymentDate(), payments.getAmount());
	}
	
	@Transactional
	public List<PaymentsDTO> findAll() 
	{
		List<PaymentsDTO> ret = new ArrayList<PaymentsDTO>();		
		PaymentsPKDTO paymentsPKDTO = null;
		for(Payments payments : paymentsRepository.findAll())
		{
			paymentsPKDTO = new PaymentsPKDTO(payments.getPaymentsPK().getCustomerNumber(), payments.getPaymentsPK().getCheckNumber());			
			ret.add(new PaymentsDTO(paymentsPKDTO, payments.getPaymentDate(), payments.getAmount()));		
		}		
		return ret;		
	}


}
