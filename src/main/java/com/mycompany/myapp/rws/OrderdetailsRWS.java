package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.OrderdetailsDTO;
import com.mycompany.myapp.entity.Orderdetails;
import com.mycompany.myapp.service.OrderdetailsService;


@RestController
@CrossOrigin(origins = "http://localhost:8091")
public class OrderdetailsRWS 
{
	
	private static final Logger logger = LoggerFactory.getLogger(OrderdetailsRWS.class);
	
	@Autowired 
	OrderdetailsService orderdetailsService;
	
	@RequestMapping(value = "/orderdetail/{orderNumber}/{checkNumber}", method = RequestMethod.GET)
	public ResponseEntity<OrderdetailsDTO> findOne(@PathVariable Integer orderNumber, @PathVariable String checkNumber)
	{
		logger.info("Listing orderdetail with orderNumer: " + orderNumber + " - " + checkNumber); 
		return new ResponseEntity<OrderdetailsDTO>(orderdetailsService.findOne(orderNumber, checkNumber), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/orderdetails", method = RequestMethod.GET)
	public ResponseEntity<List<Orderdetails>> findAll() {
		logger.info("Listing all orderdetails");
		return new ResponseEntity<List<Orderdetails>>(orderdetailsService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/orderdetails/{pageNum}/{pageSize}", method = RequestMethod.GET)
	public ResponseEntity<Page<Orderdetails>> findAllWithPaging(@PathVariable Integer pageNum, @PathVariable Integer pageSize) {
		logger.info("Listing all orderdetails with paging");
		return new ResponseEntity<Page<Orderdetails>>(orderdetailsService.findAllWithPaging(pageNum, pageSize), HttpStatus.OK);
	}
	
	

}
