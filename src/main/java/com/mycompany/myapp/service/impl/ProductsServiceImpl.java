package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.ProductsDTO;
import com.mycompany.myapp.entity.Products;
import com.mycompany.myapp.repository.ProductsRepository;
import com.mycompany.myapp.service.ProductsService;

@Service
public class ProductsServiceImpl implements ProductsService 
{	
	@Autowired
	ProductsRepository productsRepository;

	@Transactional
	public long count() 
	{		
		return productsRepository.count();		
	}
	
	@Transactional
	public ProductsDTO findOne(String id) 
	{		
		Products product = productsRepository.findOne(id);	
		return new ProductsDTO(product.getProductCode(), product.getProductName(), product.getProductScale(), product.getProductVendor(), product.getProductDescription(), product.getQuantityInStock(), product.getBuyPrice(), product.getMsrp());
	}
	
	@Transactional
	public List<ProductsDTO> findAll() 
	{
		List<ProductsDTO> ret = new ArrayList<ProductsDTO>();
		for(Products product : productsRepository.findAll())
		{
			ret.add(new ProductsDTO(product.getProductCode(), product.getProductName(), product.getProductScale(), product.getProductVendor(), product.getProductDescription(), product.getQuantityInStock(), product.getBuyPrice(), product.getMsrp()));		
		}		
		return ret;		
	}

}
