package com.mycompany.myapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassicmodelsSpringRestJpaMysqlBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClassicmodelsSpringRestJpaMysqlBeApplication.class, args);
	}
}
